from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^bckdr/', admin.site.urls),
    url(r'^tour/', include('landing.urls')),
    url(r'^', include('sharing.urls', namespace='landing')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # Debug Toolbar Stuff
    # import debug_toolbar
    # urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)),]
