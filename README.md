# OMIS (One More Image Share)

[https://omis.rocks](https://omis.rocks)

The web app for sharing images.

OMIS app is one more image share on the Web.

View incog. Sign up or log in to upload and kudos. No fuss, no muss.

Copy any image URL to a clipboard for sharing.

OMIS app is a cool mate for any of your gadgets—it’s hella adaptive.

========

Click any thumbnail to see a large image.

Feel free to use arrow keys for sliding across a selected stream.

Click Star icon to star an image you like.

Click Share icon to copy shrtimgURL to a clipboard and paste it wherever you want.

Click a username link to see a whole set of images of a particular user.

Visit your cabinet for both to manage your uploads and find what you faved.

You’re not allowed to star own images.

Anonymous users only may view pics.