//
//  OMIS Sharing app functions
//
console.log('Hello from app/sharing.js');

// File select feedback
$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '').replace(/\.[^/.]+$/, '');
    input.trigger('fileselect', [label]);
  });

  $(document).ready( function() {
      $(':file').on('fileselect', function(event, label) {
          var input = $(this).parents('.input-group').find(':text'),
              log = label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
  });
});


// clipboard.js initialization
$(function () {
  new Clipboard('#clipBtn');
});

// Bootstrap 3 tooltips stuff
$(document).ready(function(){
    $('.show-tooltip').on('click touch', function(){
        $('#shareTooltip').tooltip('show');
    });
});


// Hide tooltip
$(function () {
   $(document).on('shown.bs.tooltip', function (e) {
      setTimeout(function () {
        $(e.target).tooltip('hide');
      }, 1500);
   });
});


// Key navigation
$(document).keyup(function(e) {
    switch(e.which) {
        case 40: // bottom
        case 37: // left
        $('a#nextImage')[0].click();
        break;

        case 38: // top
        case 39: // right
        $('a#prevImage')[0].click();
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});


// Resize img-poster container function
$(document).ready(function(){
    resizeDiv();
});

window.onresize = function() {
    resizeDiv();
};

function resizeDiv() {
    var vph = 0,
        menu_h = $('div.container-fluid').height(),
        status_h = $('div.statusbar').height(),
        footer_h = $('footer.navbar-fixed-bottom').height();

    if ($(window).height() < 500) {
        vph = $(window).height();
    } else {
        vph = $(window).height() - menu_h - status_h - footer_h - 110;
    }

$('.img-poster').css({'height': vph});
}


// Alert autohide function
$('#alert-dismiss').delay(3500).fadeOut(500, function() {
    $(this).alert('close');
});


// POST AJAX Star button implementation
$('i#starred').on('click', function(event){
    event.preventDefault();
    var element = $(this);

    $.ajax({
        url : '/starred/',
        type : 'POST',
        data : {slug : element.attr('data-id')},

        success : function(data){
            $('#s-counter').html(data.star_count);
            if (data.star_flag) {
                $("i#starred").removeClass('fa-star-o').addClass('fa-star');
            } else {
                $("i#starred").removeClass('fa-star').addClass('fa-star-o');
            }
        }
    });
});


// Add the CSRF token using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
