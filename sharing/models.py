from django.contrib.sessions.models import Session
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db import models
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill, ResizeToFit
from time import strftime

from .tools import *
from .validators import *

PROC_IMG_W = 2048
PROC_IMG_H = 2048
PROC_IMG_Q = 90
SRC_IMG_MAX = 7340032  # In bytes. Count as xMB * 1024 ^ 2
THMB_W = 256
THMB_H = 256
THMB_Q = 75
IMG_EXT = ('jpeg', 'jpg', 'gif', 'png', 'tif', 'tiff')
IMG_MIME = ('image/jpeg', 'image/gif', 'image/png', 'image/tiff')


def update_filename(instance, filename):
    """
    The function is for build full filename
    with folder and new filename
    for 'upload_to' attribute
    :return: filepath with filename
    """
    folder = strftime('%H')
    new_filename = instance.slug
    ext = filename.split('.')[-1]
    return '{}/{}.{}'.format(folder, new_filename, ext)


class Image(models.Model):
    """
    Image model
    """
    img = ProcessedImageField(upload_to=update_filename,
                              processors=[ResizeToFit(PROC_IMG_W, PROC_IMG_H, False)],
                              format='JPEG',
                              options={'quality': PROC_IMG_Q},
                              validators=[FileValidator(
                                  max_size=SRC_IMG_MAX,
                                  allowed_extensions=IMG_EXT,
                                  allowed_mimetypes=IMG_MIME)])
    img_thmb = ImageSpecField(source='img',
                              processors=[ResizeToFill(THMB_W, THMB_H)],
                              format='JPEG',
                              options={'quality': THMB_Q})
    desc = models.CharField('Title',
                            max_length=100,
                            help_text='Up to 100 char.',
                            blank=True)
    slug = models.SlugField(max_length=10, unique=True, db_index=True)
    upl_date = models.DateTimeField('Uploaded', auto_now_add=True, db_index=True)
    rev_date = models.DateTimeField('Last reviewed', null=True)
    rev_count = models.PositiveIntegerField('Reviews', default=0)
    star_count = models.PositiveIntegerField('Stars', default=0)
    user = models.ForeignKey(User, default=1, db_index=True)

    # def save(self, *args, **kwargs):
    #     """
    #      Override to add a slug to new image during save
    #     """
    #     if not self.slug:
    #         self.slug = gen_slug()
    #     super(Image, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('sharing:detail', kwargs={'slug': self.slug})

    def delete(self, *args, **kwargs):
        """
        Override to clean up a garbage upon an object deletion
        """
        # TODO: Wait for CACHE deletion method
        self.img.delete(save=False)
        super(Image, self).delete(*args, **kwargs)

    def __str__(self):
        return '{} - {}'.format(self.slug, self.desc)


class Starred(models.Model):
    """
    Model for unique star count
    """
    image = models.ForeignKey(Image, db_index=True, blank=True)
    user = models.ForeignKey(User, default=1, db_index=True)
    star_date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return '{} - {}'.format(self.image_id, self.user)


class Seen(models.Model):
    """
    Model for unique image review count
    """
    image = models.ForeignKey(Image, db_index=True, blank=True)
    session = models.TextField(db_index=True, blank=False)

    def __str__(self):
        return '{} - {}'.format(self.image_id, self.session)
