from django.contrib import admin
from .models import Image, Starred, Seen

admin.site.register(Image)
admin.site.register(Starred)
admin.site.register(Seen)
