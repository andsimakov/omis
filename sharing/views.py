from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import F
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View

from .forms import UserForm, ImageForm
from .models import Image, Starred, Seen
from .tools import *

# Initial constants
IMAGE_COUNT = 6
IMAGE_COUNT_PAGINATOR = IMAGE_COUNT * 6


def index(request):
    """
    Main page view
    """
    if 'tour' not in request.COOKIES:
        response = redirect('tour')
        response.set_cookie('tour', 'False')
        return response

    else:
        if request.method == 'POST':
            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                image = form.save(commit=False)
                image.user = request.user
                image.slug = gen_slug()
                image.save()
                messages.add_message(request, messages.SUCCESS, _('Yee haw! Your amazing image uploaded'))
                return redirect('sharing:index')
            else:
                # Clear image title if not valid
                form.data.pop('desc')
        else:
            form = ImageForm()

        images_recent = Image.objects.order_by('-upl_date')[:IMAGE_COUNT]
        images_popular = Image.objects.order_by('-rev_count')[:IMAGE_COUNT]
        images_star = Image.objects.order_by('-star_count')[:IMAGE_COUNT]
        context = {
            'form': form,
            'images_recent': images_recent,
            'images_popular': images_popular,
            'images_star': images_star
        }
        return render(request, 'sharing/index.html', context)


class UserFormView(View):
    """
    User signup form
    """
    form_class = UserForm
    template_name = 'sharing/signup.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            messages.add_message(request, messages.SUCCESS, _('Welcome to OMIS!'))
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('sharing:index')

        return render(request, self.template_name, {'form': form})


def login_user(request):
    """
    User login view
    """
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.add_message(request, messages.INFO, 'Namaste, stargazer!')
                return redirect('sharing:index')
                # TODO: Fix inactive user behavior here. It needs to write custom authenticate()
                # else:
                #     return render(request, 'sharing/login.html', {'error_message': 'The Account Has Been Disabled'})
        else:
            msg = _('We don’t recognize you')
            return render(request, 'sharing/login.html', {'error_message': msg})
    return render(request, 'sharing/login.html')


def logout_user(request):
    """
    User logout view
    """
    logout(request)
    messages.add_message(request, messages.INFO, _('{}! We will miss you'.format(greetwell('bye'))))
    return redirect('sharing:index')


def show_page(request, page):
    """
     Show Image set view
    """
    if '/popular/' in request.path:
        images = Image.objects.order_by('-rev_count')
    elif '/star/' in request.path:
        images = Image.objects.order_by('-star_count')
    else:
        images = Image.objects.order_by('-upl_date')

    paginator = Paginator(images, IMAGE_COUNT_PAGINATOR)
    try:
        images = paginator.page(page)
    except PageNotAnInteger:
        images = paginator.page(1)
    except EmptyPage:
        images = paginator.page(paginator.num_pages)
    return render(request, 'sharing/set.html', {'images': images})


def show_user(request, user, page):
    """
    Show images by user view
    """
    if user is not None:
        try:
            user_id = User.objects.get(username=user)
        except User.DoesNotExist:
            messages.add_message(request, messages.ERROR, _('We aren’t familiar with an artist'))
            return redirect('sharing:index')

        if '/starred/' in request.path:
            # TODO: Add this queryset ordering by '-star_date'
            user_image_set = \
                Image.objects.filter(id__in=Starred.objects.filter(user=user_id).values('image')).order_by('-upl_date')
            stars = True
        else:
            user_image_set = Image.objects.filter(user=user_id).order_by('-upl_date')
            stars = False

        paginator_image_set = list(user_image_set)

        paginator = Paginator(paginator_image_set, IMAGE_COUNT_PAGINATOR)
        try:
            images = paginator.page(page)
        except PageNotAnInteger:
            images = paginator.page(1)
        except EmptyPage:
            images = paginator.page(paginator.num_pages)
        return render(request, 'sharing/set.html', {
            'stars': stars,
            'greeting': greetwell('hi'),
            'user_name': user,
            'images': images
        })
    else:
        messages.add_message(request, messages.ERROR, _('No artist specified'))
        return redirect('sharing:index')


def detail(request, slug, sorting):
    """
    Image detail view
    """
    # Get a previous URL or get home
    try:
        previous_page = request.META['HTTP_REFERER']
    except KeyError:
        previous_page = '/'

    image = get_object_or_404(Image, slug=slug)
    star_flag = Starred.objects.filter(image=image.id, user=request.user.id).count()

    if sorting == 'popular':
        order = '-rev_count'
    elif sorting == 'star':
        order = '-star_count'
    elif sorting == 'upload':
        order = '-upl_date'
    elif sorting == 'user':
        order = '-upl_date'
    else:
        order = '-id'

    if sorting == 'user':
        images = Image.objects.filter(user=image.user_id).order_by(order)
    else:
        images = Image.objects.order_by(order)

    _count = 0
    for _item in images:
        if _item == image:
            break
        else:
            _count += 1

    if order == '-id':
        next_image = prev_image = None
    else:
        next_image = images[_count - 1].slug if _count > 0 else None
        prev_image = images[_count + 1].slug if _count < (len(images) - 1) else None

    request.session.set_expiry(60 * 60 * 24)

    if str(request.user) is not 'AnonymousUser':
        session_key = request.user.id
    else:
        request.session.save()
        session_key = request.session.session_key

    image_seen = Seen.objects.filter(image=image.id, session=session_key).exists()

    if not image_seen:
        session = Seen()
        session.image_id = image.id
        session.session = session_key
        session.save()

        image.rev_count = F('rev_count') + 1
        image.rev_date = timezone.now()
        image.save()
        image.refresh_from_db()

    try:
        user_name = User.objects.get(id=image.user_id)
    except User.DoesNotExist:
        user_name = 'unknown'

    return render(request, 'sharing/detail.html', {
        'star_flag': star_flag,
        'footer': 'nofooter',
        'sorting': sorting,
        'next_image': next_image,
        'prev_image': prev_image,
        'previous_page': previous_page,
        'image': image,
        'user_name': user_name
    })


def starred(request):
    """
    User-dependent AJAX-based star counter
    """
    slug = request.POST.get('slug', None)
    # star_count = 0
    star_flag = False

    if slug:
        image = Image.objects.get(slug=slug)
        if image:
            star_flag = Starred.objects.filter(image=image.id, user=request.user.id).count()

            if star_flag:
                Starred.objects.filter(image=image.id, user=request.user.id).delete()
                star_flag = False
                if image.star_count > 0:
                    image.star_count = F('star_count') - 1
            else:
                star = Starred()
                star.image_id = image.id
                star.user = request.user
                star.save()
                star_flag = True
                image.star_count = F('star_count') + 1

            image.save()
            image.refresh_from_db()

    return JsonResponse({'star_count': image.star_count, 'star_flag': star_flag})


def delete(request, slug):
    """
     Delete image view
    """
    image = Image.objects.get(slug=slug)
    image.delete()
    messages.add_message(request, messages.SUCCESS, _('Your image deleted'))
    # Get logged in user for correct redirect to a cabinet
    user = str(request.user)
    return redirect('sharing:user', user)
