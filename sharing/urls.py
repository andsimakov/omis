from django.conf.urls import url
from . import views

app_name = 'sharing'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup/$', views.UserFormView.as_view(), name='signup'),
    url(r'^all/(?P<page>[\d]+)?$', views.show_page, name='all'),
    url(r'^popular/(?P<page>[\d]+)?$', views.show_page, name='popular'),
    url(r'^star/(?P<page>[\d]+)?$', views.show_page, name='star'),
    url(r'^starred/$', views.starred, name='starred'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^u/(?P<user>[\w]+)?/?(starred)?/?(?P<page>[\d]+)?$', views.show_user, name='user'),
    url(r'^(?P<slug>[\w]+)/(?P<sorting>[\w]+)?$', views.detail, name='detail'),
    url(r'^(?P<slug>[\w]+)/del/$', views.delete, name='del'),
]