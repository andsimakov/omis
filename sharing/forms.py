from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .models import Image


class ImageForm(forms.ModelForm):
    # 'Upload a valid image. The file you uploaded was either not an image or a corrupted image.'
    msg = _('Not allowed file type or corrupted file')
    img = forms.ImageField(error_messages={'invalid_image': msg})

    class Meta:
        model = Image
        fields = ['img', 'desc']
        widgets = {
            'desc': forms.TextInput(attrs={
                'autocomplete': 'off',
                'class': 'form-control text-center',
                'placeholder': 'Image Title'
            }),
        }


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'password']
        msg = _('Oops. We already have one here')
        error_messages = {
            'username': {
                'unique': msg,
            },
        }

        css_class = 'form-control'
        widgets = {
            'email': forms.EmailInput(attrs={
                'autocomplete': 'off',
                'class': css_class,
                'required': 'true',
            }),
            'username': forms.TextInput(attrs={
                'class': css_class,
            }),
            'password': forms.PasswordInput(attrs={
                'class': css_class,
            }),
        }
