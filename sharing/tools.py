from random import randint
from django.utils.baseconv import base56


def gen_slug():
    """
    Image unique slug generator
    :return: unique slug string
    """
    return base56.encode(randint(0, 0xffffffffff))


def greetwell(phrase):
    """
    Returns random greeting/farewell from a list of greetings
    :arg, phrase, str 'hi', 'bye'
    :return str, greeting/farewell
    """
    greetings = ['Aloha', 'Hi', 'Halo', 'Hola',
                 'Shalom', 'Kaixo', 'Fáilte',
                 'Hej', 'Merhaba', 'Pryvit',
                 'Szia', 'Hei', 'Hoi', 'Hey',
                 'Bonjour', 'Bok', 'Ahoj', 'Meow',
                 'Namaskara', 'Yasou', 'Góðan daginn',
                 'Dobrý den', 'Gruß', 'Hello',
                 'Konnichi wa', 'Witaj', 'Servus',
                 'G’day', 'Zdraveite', 'Ave',
                 'Moien', 'Ola', 'Dydd da',
                 'Paivaa', 'Zdravo', 'Guid mornin',
                 'Saluton', 'Buna', 'Sveikas',
                 'Haa', 'Chao', 'Salut']

    farewells = ['Xudaafiz', 'Seeyabye', 'Goodbye',
                 'Da pabaczenia', 'Nomoskaar', 'Dovizhdane',
                 'Dovidjenja', 'Chao', 'Tot ziens',
                 'Khairete', 'Nakhvamdis', 'Farvel',
                 'Lehit', 'Guddy vaj', 'Bless',
                 'Hasta la vista', 'Arrivederci',
                 'Aquetan', 'Toq', 'Vale', 'Eddi',
                 'Dogledanje', 'Vastomazonok',
                 'Tschuß', 'Auf Wiedersehen',
                 'Farvel', 'Do widzenia', 'Ate a vista',
                 'La revedere', 'Do videnja', 'Addiu',
                 'Do pobachennya', 'Hwyl', 'Nakemiin',
                 'Au revoir', 'Pirmelenge', 'Zbohem',
                 'Adjo', 'Guidbye', 'Sayonara']

    if phrase == 'hi':
        return greetings[randint(0, len(greetings) - 1)]
    elif phrase == 'bye':
        return farewells[randint(0, len(farewells) - 1)]
    else:
        return 'Foobar ;)'
