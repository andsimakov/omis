# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-28 10:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sharing', '0022_auto_20170228_1241'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seen',
            name='image',
            field=models.IntegerField(db_index=True),
        ),
    ]
