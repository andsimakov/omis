# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-23 14:23
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sharing', '0008_starred_star_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='starred',
            name='user_id',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
