# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-13 19:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('sharing', '0007_auto_20170213_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='starred',
            name='star_date',
            field=models.DateTimeField(auto_now_add=True, db_index=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
