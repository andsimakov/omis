# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-03 11:26
from __future__ import unicode_literals

from django.db import migrations, models
import imagekit.models.fields
import sharing.models
import sharing.validators


class Migration(migrations.Migration):

    dependencies = [
        ('sharing', '0002_auto_20170113_0021_squashed_0026_auto_20170228_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='img',
            field=imagekit.models.fields.ProcessedImageField(upload_to=sharing.models.update_filename, validators=[sharing.validators.FileValidator(allowed_extensions=('jpeg', 'jpg', 'gif', 'png', 'tif', 'tiff'), allowed_mimetypes=('image/jpeg', 'image/gif', 'image/png', 'image/tiff'), max_size=7340032)]),
        ),
        migrations.AlterField(
            model_name='starred',
            name='star_date',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
    ]
