from django.core.management import BaseCommand
from datetime import datetime
import shutil

from omis.settings import *
from ...models import Image


class Command(BaseCommand):
    help = 'Removes orphan thumbnails after main images deletion. Django Imagekit isn’t capable.'

    def handle(self, *args, **options):
        thmb_path = '{}/CACHE/images'.format(MEDIA_ROOT)
        images = Image.objects.all().values_list('slug', flat=True)

        counter = 0
        for folder in os.listdir(thmb_path):
            for subfolder in os.listdir('{}/{}'.format(thmb_path, folder)):
                if subfolder not in images:
                    shutil.rmtree('{}/{}/{}'.format(thmb_path, folder, subfolder), ignore_errors=False)
                    counter += 1

        module_name = __name__.split('.')[-1]
        timestamp = datetime.now().strftime('%Y-%m-%d, %H:%M:%S')
        output = '{}: {}: {} orphan thumbnail(s) removed.'
        self.stdout.write(output.format(timestamp, module_name, counter))

