from django.core.management import BaseCommand
from datetime import datetime

from ...models import Image, Starred


class Command(BaseCommand):
    help = 'Syncs stars of Image model with Starred model.'

    def handle(self, *args, **options):
        images = Image.objects.all().values_list('id', flat=True)
        counter = 0
        for image_id in images:
            sharing = Image.objects.get(id=image_id)
            actual = Starred.objects.filter(image_id=image_id).count()
            if sharing.star_count != actual:
                sharing.star_count = actual
                sharing.save()
                sharing.refresh_from_db()
                counter += 1

        module_name = __name__.split('.')[-1]
        timestamp = datetime.now().strftime('%Y-%m-%d, %H:%M:%S')
        output = '{}: {}: {} out-of-sync record(s) fixed.'
        self.stdout.write(output.format(timestamp, module_name, counter))
